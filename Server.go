package srs

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

type Server struct {
	e *echo.Echo
}

func NewServer(debug bool) *Server {
	e := echo.New()
	e.Debug = debug
	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	return &Server{e}
}

func (s *Server) Use(m echo.MiddlewareFunc) {
	s.e.Use(m)
}

func (s *Server) Start(port string) {
	s.e.Logger.Fatal(s.e.Start(":" + port))
}

func (r *Router) Group(path string, middleware ...echo.MiddlewareFunc) *Group {
	return &Group{r.e.Group(path, middleware...)}
}

type Router struct {
	e *echo.Echo
}

func (s *Server) NewRouter() *Router {
	return &Router{s.e}
}

type Group struct {
	g *echo.Group
}

func (g *Group) Group(prefix string, middleware ...echo.MiddlewareFunc) *Group {
	return &Group{g.g.Group(prefix, middleware...)}
}

func (g *Group) AddJWTMiddleware(secret string) {
	g.g.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		Claims:        &JwtCustomClaims{},
		SigningKey:    []byte(secret),
		SigningMethod: "HS512",
	}))
}

func (g *Group) CONNECT(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.CONNECT(path, handler, m...)
}
func (g *Group) DELETE(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.DELETE(path, handler, m...)
}
func (g *Group) GET(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.GET(path, handler, m...)
}
func (g *Group) HEAD(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.HEAD(path, handler, m...)
}
func (g *Group) OPTIONS(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.OPTIONS(path, handler, m...)
}
func (g *Group) PATCH(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.PATCH(path, handler, m...)
}
func (g *Group) POST(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.POST(path, handler, m...)
}
func (g *Group) PUT(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.PUT(path, handler, m...)
}
func (g *Group) TRACE(path string, handler echo.HandlerFunc, m ...echo.MiddlewareFunc) {
	g.g.TRACE(path, handler, m...)
}
