package srs

type ServiceInterface interface {
	Get(id string) (interface{}, error)
	List(queryOptions QueryOptions, sort Sort) (interface{}, error)
	Create(b BindFunc) (interface{}, error)
	Update(id string, b BindFunc) (interface{}, error)
	Delete(id string) (bool, error)
}

type BindFunc func(interface{}) error

type QueryOptions struct {
	Limit  int                    `json:"limit"`
	Offset int                    `json:"offset"`
	Query  map[string]interface{} `json:"query"`
}

type Sort struct {
	Field   string
	Reverse bool
}

func NewQueryOptions() QueryOptions {
	return QueryOptions{0, 0, make(map[string]interface{}, 0)}
}

type CollectionInterface interface {
	CollectionName() string
}

type PrivateServiceInterface interface {
	CollectionInterface
	ServiceInterface
	SetOwner(v interface{})
	ResetOwner()
}

type PrivateService struct {
	Owner interface{}
}

func (s *PrivateService) SetOwner(v interface{}) {
	s.Owner = v
}

func (s *PrivateService) ResetOwner() {
	s.Owner = nil
}
