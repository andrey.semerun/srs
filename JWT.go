package srs

import (
	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type JwtCustomClaims struct {
	Id    primitive.ObjectID `json:"id"`
	Email string             `json:"email"`
	Admin bool               `json:"admin"`
	jwt.StandardClaims
}
