package srs

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"github.com/buger/jsonparser"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Controller struct {
	Service ServiceInterface
}

func NewController(s ServiceInterface) *Controller {
	return &Controller{s}
}

type ApiControllerInterface interface {
	Create(c echo.Context)
	List(c echo.Context)
	Get(c echo.Context)
	Update(c echo.Context)
	Delete(c echo.Context)
}

type ApiController struct {
	service ServiceInterface
}

func NewApiController(s ServiceInterface) *ApiController {
	return &ApiController{s}
}

func (ac ApiController) Create(c echo.Context) error {

	item, err := ac.service.Create(c.Bind)

	if err != nil {
		errorCode := http.StatusInternalServerError
		c.JSON(errorCode, struct {
			Code  int    `json:"code"`
			Error string `json:"error"`
		}{
			errorCode,
			err.Error(),
		})
	}

	return c.JSON(http.StatusCreated, item)
}

func (ac ApiController) Update(c echo.Context) error {

	item, err := ac.service.Update(c.Param("id"), c.Bind)
	if item == nil || err != nil {
		errorMsg := http.StatusText(http.StatusNotFound)
		errorCode := http.StatusNotFound
		if err != nil {
			errorMsg = err.Error()
			errorCode = http.StatusInternalServerError
		}
		c.JSON(errorCode, struct {
			Code  int    `json:"code"`
			Error string `json:"error"`
		}{
			errorCode,
			errorMsg,
		})
		return fmt.Errorf("[%d] %s", errorCode, errorMsg)
	}

	return c.JSON(http.StatusOK, item)
}

func (ac ApiController) List(c echo.Context) error {

	queryOptions := NewQueryOptions()
	queryOptions.Limit, _ = strconv.Atoi(c.QueryParam("limit"))
	queryOptions.Offset, _ = strconv.Atoi(c.QueryParam("offset"))
	jsonparser.ObjectEach([]byte(c.QueryParam("query")), func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
		pattern, _ := url.QueryUnescape(string(value))
		queryOptions.Query[string(key)] = primitive.Regex{Pattern: pattern, Options: "i"}
		return nil
	})

	switch true {
	case queryOptions.Limit < 1:
		queryOptions.Limit = 10
	case queryOptions.Limit > 50:
		queryOptions.Limit = 50
	}

	if queryOptions.Offset < 0 {
		queryOptions.Offset = 0
	}

	var sort Sort
	if c.QueryParam("sort") != "" {
		jsonparser.ObjectEach([]byte(c.QueryParam("sort")), func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
			sort.Field = string(key)
			if string(value) == "-1" {
				sort.Reverse = true
			}
			return nil
		})
	}

	items, err := ac.service.List(queryOptions, sort)

	if err != nil {
		errorCode := http.StatusInternalServerError
		c.JSON(errorCode, struct {
			Code  int    `json:"code"`
			Error string `json:"error"`
		}{errorCode, err.Error()})
		return err
	}

	return c.JSON(http.StatusOK, items)
}

func (ac ApiController) Get(c echo.Context) (err error) {

	item, err := ac.service.Get(c.Param("id"))

	if item == nil || err != nil {
		errorMsg := http.StatusText(http.StatusNotFound)
		errorCode := http.StatusNotFound
		if err != nil {
			errorMsg = err.Error()
			errorCode = http.StatusInternalServerError
		}
		c.JSON(errorCode, struct {
			Code  int    `json:"code"`
			Error string `json:"error"`
		}{
			errorCode,
			errorMsg,
		})
		return fmt.Errorf("[%d] %s", errorCode, errorMsg)
	}

	return c.JSON(http.StatusOK, item)
}

func (ac ApiController) Delete(c echo.Context) (err error) {

	id := c.Param("id")

	result, err := ac.service.Delete(id)

	if !result || err != nil {
		errorMsg := http.StatusText(http.StatusNotFound)
		errorCode := http.StatusNotFound
		if err != nil {
			errorMsg = err.Error()
			errorCode = http.StatusInternalServerError
		}
		c.JSON(errorCode, struct {
			Code  int    `json:"code"`
			Error string `json:"error"`
		}{
			errorCode,
			errorMsg,
		})
		return fmt.Errorf("[%d] %s", errorCode, errorMsg)
	}

	return c.JSON(http.StatusOK, id)
}

type PrivateController struct {
	service PrivateServiceInterface
	ApiController
}

func NewPrivateController(s PrivateServiceInterface) *PrivateController {
	controller := &PrivateController{service: s}
	controller.ApiController.service = controller.service
	return controller
}

func (ctrl *PrivateController) setOwner(c echo.Context) *PrivateController {
	ctrl.service.SetOwner(((c.Get("user").(*jwt.Token)).Claims.(*JwtCustomClaims)).Id)
	return ctrl
}

func (ctrl PrivateController) Get(c echo.Context) error {
	defer ctrl.service.ResetOwner()
	return ctrl.setOwner(c).ApiController.Get(c)
}

func (ctrl PrivateController) List(c echo.Context) error {
	defer ctrl.service.ResetOwner()
	return ctrl.setOwner(c).ApiController.List(c)
}

func (ctrl PrivateController) Create(c echo.Context) error {
	defer ctrl.service.ResetOwner()
	return ctrl.setOwner(c).ApiController.Create(c)
}

func (ctrl PrivateController) Update(c echo.Context) error {
	defer ctrl.service.ResetOwner()
	return ctrl.setOwner(c).ApiController.Update(c)
}

func (ctrl PrivateController) Delete(c echo.Context) error {
	defer ctrl.service.ResetOwner()
	return ctrl.setOwner(c).ApiController.Delete(c)
}

type RestrictedController struct {
	service        ServiceInterface
	accessResolver func(c echo.Context) bool
	ApiController
}

func NewRestrictedController(s ServiceInterface, r func(c echo.Context) bool) *RestrictedController {
	controller := &RestrictedController{service: s, accessResolver: r}
	controller.ApiController.service = controller.service
	return controller
}

func (ctrl RestrictedController) Get(c echo.Context) error {
	if !ctrl.accessResolver(c) {
		return c.JSON(http.StatusForbidden, http.StatusText(http.StatusForbidden))
	}
	return ctrl.ApiController.Get(c)
}

func (ctrl RestrictedController) List(c echo.Context) error {
	if !ctrl.accessResolver(c) {
		return c.JSON(http.StatusForbidden, http.StatusText(http.StatusForbidden))
	}
	return ctrl.ApiController.List(c)
}

func (ctrl RestrictedController) Create(c echo.Context) error {
	if !ctrl.accessResolver(c) {
		return c.JSON(http.StatusForbidden, http.StatusText(http.StatusForbidden))
	}
	return ctrl.ApiController.Create(c)
}

func (ctrl RestrictedController) Update(c echo.Context) error {
	if !ctrl.accessResolver(c) {
		return c.JSON(http.StatusForbidden, http.StatusText(http.StatusForbidden))
	}
	return ctrl.ApiController.Update(c)
}

func (ctrl RestrictedController) Delete(c echo.Context) error {
	if !ctrl.accessResolver(c) {
		return c.JSON(http.StatusForbidden, http.StatusText(http.StatusForbidden))
	}
	return ctrl.ApiController.Delete(c)
}
